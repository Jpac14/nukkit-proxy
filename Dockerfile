# Copyright (C) 2021 Joshua Pacifici <jpac14@outlook.com> 
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU Affero General Public License as published
# by the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
# GNU Affero General Public License for more details.
#
# You should have received a copy of the GNU Affero General Public License
# along with this program. If not, see <http://www.gnu.org/licenses/>.

FROM cubxity/minecraft-proxy:velocity-adopt16

COPY --chown=minecraft:minecraft config/velocity.toml /data/velocity.toml
COPY --chown=minecraft:minecraft config/floodgate.yml /data/plugins/floodgate/config.yml
COPY --chown=minecraft:minecraft config/geyser.yml /data/plugins/Geyser-Velocity/config.yml
COPY --chown=minecraft:minecraft config/viabackwards.yml /data/plugins/viabackwards/config.yml/config.yml
COPY --chown=minecraft:minecraft config/viarewind.yml /data/plugins/viarewind/config.yml
COPY --chown=minecraft:minecraft config/viaversion.yml /data/plugins/viaversion/config.yml

RUN wget -P /data/plugins https://ci.opencollab.dev/job/GeyserMC/job/Floodgate/job/master/24/artifact/velocity/target/floodgate-velocity.jar 

RUN wget -P /data/plugins https://ci.opencollab.dev/job/GeyserMC/job/Geyser/job/master/829/artifact/bootstrap/velocity/target/Geyser-Velocity.jar 

RUN wget -P /data/plugins https://ci.viaversion.com/view/ViaBackwards/job/ViaBackwards/229/artifact/build/libs/ViaBackwards-4.0.2-SNAPSHOT.jar 

RUN wget -P /data/plugins https://ci.viaversion.com/view/ViaRewind/job/ViaRewind/114/artifact/all/target/ViaRewind-2.0.2-SNAPSHOT.jar 

RUN wget -P /data/plugins https://ci.viaversion.com/job/ViaVersion/lastBuild/artifact/build/libs/ViaVersion-4.0.2-SNAPSHOT.jar 